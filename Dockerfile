FROM ubuntu:16.04
MAINTAINER "luan@fosec.vn"

USER root
RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y python-pip && pip install --upgrade pip

WORKDIR /opt

ADD ./requirements.txt /opt/requirements.txt
ADD ./flask_extensions/ /opt/flask_extensions/

RUN pip install supervisor \
    && pip install -r requirements.txt

ADD ./ /opt/vsm-api

RUN cd /opt/flask_extensions/Flask-CAS-1.0.0 && python setup.py install \
    && cd /opt/flask_extensions/Flask-Oauth2-1.0.1 && python setup.py install \
    && cd /opt/flask_extensions/Flask-Permissions-0.10.2 && python setup.py install

EXPOSE 8090

CMD ["supervisord", "-n", "-c", "/opt/vsm-api/docker-config/supervisord.conf"]