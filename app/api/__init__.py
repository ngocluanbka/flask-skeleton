from flask import Blueprint
from . import demo


def init_app(app, **kwargs):
    # pylint: disable=unused-argument
    api_v1_blueprint = Blueprint('api', __name__, url_prefix='/api/v1')

    # todo.api.init_app(api_v1_blueprint)
    demo.api.init_app(api_v1_blueprint)

    app.register_blueprint(api_v1_blueprint)