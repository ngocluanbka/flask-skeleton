# -*- coding: utf-8 -*-
__author__ = 'luan'
import os
from flask import session
from app.lib.flask_restplus_patched import Resource
from werkzeug.exceptions import Unauthorized, BadRequest, InternalServerError
from app.lib.common import schema
from app.lib.common.utils import pack_result
from ..extensions import api

ns = api.namespace('demo')


@ns.route('/')
class ListAPIDemo(Resource):
    @ns.parameters(schema.DemoListParams())
    def get(self, args, **kwargs):
        # do get action
        return pack_result(data=args)

    @ns.parameters(schema.DemoCreateParams(), locations=('json',))
    def post(self, args, **kwargs):
        # do post action
        return pack_result(message="Done")


@ns.route('/<int:id>')
class ItemAPIDemo(Resource):
    @ns.parameters(schema.DemoGetItemParams())
    def get(self, args, **kwargs):
        id = kwargs.get('id')
        # do get action
        return pack_result(data={'id': id})

    @ns.parameters(schema.DemoUpdateParams(), locations=('json',))
    def put(self, args, **kwargs):
        id = kwargs.get('id')
        # do put action
        return pack_result(data={'id': id})

    @ns.parameters(schema.DemoGetItemParams(), locations=('json',))
    def put(self, args, **kwargs):
        id = kwargs.get('id')
        # do delete action
        return pack_result(data={'id': id})


