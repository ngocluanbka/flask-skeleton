# -*- coding: utf-8 -*-
import os


def make_dir(dir_path):
    try:
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
    except Exception as e:
        raise e


class DefaultConfig(object):
    DEBUG = True
    FILENAME = __file__

    # Security
    # This is the secret key that is used for session signing.
    # SECRET_KEY = os.urandom(24)
    SECRET_KEY = "Fixed for test"
    # The filename for the info and error logs. The logfiles are stored at log-api/logs
    INFO_LOG = "info.log"
    ERROR_LOG = "error.log"
    current_path = os.path.dirname(__file__)
    LOG_FOLDER = os.path.join(os.path.abspath(os.path.dirname(current_path)),
                              'logs')


class ApiAppConfig(DefaultConfig):
    """Special config for Flancer API"""
    REDIS_HOST = 'localhost'
    REDIS_PORT = 6379
    REDIS_DB = 1
    REDIS_DECODE_RESPONSES = True

    # For Celery
    result_backend = 'redis://localhost:6379/1'
    broker_url = 'redis://localhost:6379/1'

    MAIL_SERVER = 'smtp server addr'
    MAIL_PORT = 465
    MAIL_USERNAME = 'username@gmail.com'
    MAIL_PASSWORD = 'password'
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True

    SQLALCHEMY_DATABASE_URI = 'mysql://root:root@localhost:3306/martutor'
    SQLALCHEMY_POOL_SIZE = 10
    SQLALCHEMY_MAX_OVERFLOW = 20

    COOKIE_MAX_AGE = 2147483647
    SESSION_TIME_DELTA = 60
    COOKIE_SECURE = False
    # COOKIE_DOMAIN = "127.0.0.1"

    USERNAME_EXP = "^[a-zA-Z][a-zA-Z0-9_]{4,29}$"
    PWD_EXP = "^((?=(.*[a-zA-Z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s)).{8,}$"
    PHONE_EXP = "^\+?[0-9 ]*$"
    IP_DOMAIN_EXP = "^(([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,10})|((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))$"


class TestingConfig(ApiAppConfig):
    TESTING = True
    DEBUG = True

    MONGO_DBNAME = 'flancer_test'
