# -*- coding: utf-8 -*-
from webargs.flaskparser import FlaskParser
from flask_sqlalchemy import SQLAlchemy
from app.lib.flask_restplus_patched import Api
# from .lib.flask_walrus import WalrusDatabase

db = SQLAlchemy()
parser = FlaskParser()
# perms = Permissions(None, db, None)
# redis_db = WalrusDatabase()

api = Api(version='1.0', title='Our Application', description="This is our website")