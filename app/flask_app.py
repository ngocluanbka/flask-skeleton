# -*- coding: utf-8 -*-
import logging
import os
import sys
from logging.handlers import RotatingFileHandler
from flask import Flask, g, render_template
from os.path import dirname, abspath, join, sep
from werkzeug.exceptions import default_exceptions
from .exceptions import api_error_handler
# from .api import auth, account, token, user, whitelist, profile
from . import api


# Config
from . import DEFAULT_CONFIG_CLASS, DEFAULT_APPLICATION_CONFIG_FILE, \
    DEFAULT_GLOBAL_CONFIG_FILE, load_module


def create_app(config=None):
    """Creates the app."""
    # Initialize the app
    path_lib = dirname(dirname(abspath(__file__))) + '/build'

    # app = Flask(__name__, static_url_path="", static_folder=abspath(path_lib),
    #                   template_folder=abspath(path_lib))

    app = Flask(__name__, static_url_path="", template_folder="")
    app.config.from_object(config)
    return app


def configure_app(app, filename=DEFAULT_GLOBAL_CONFIG_FILE,
                  config_class=DEFAULT_CONFIG_CLASS):
    """
    Configure a Flask app
    :param app:
    :param filename:
    :param config_class:
    :return:
    """
    # Select config file
    if not os.path.isfile(filename):
        filename = DEFAULT_APPLICATION_CONFIG_FILE

    # Load proper config for app
    config = load_module(filename)
    app.config.from_object(getattr(config, config_class))
    app.config.from_pyfile('./config_local.py', silent=True)

    # Correct LOG_FOLDER config
    app.config['LOG_FOLDER'] = os.path.join(
        os.path.abspath(os.path.dirname(os.path.dirname(__file__))), 'logs')
    config.make_dir(app.config['LOG_FOLDER'])

    # Config Flask components
    # configure_route(app)
    configure_extensions(app)
    configure_blueprints(app)
    configure_log_handlers(app)
    configure_error_handlers(app)
    configure_after_request(app)

    # export_swagger_specifications()


def configure_extensions(app):
    """
    :param app: flask app (main vsm_api)
    :return:
    """
    from .extensions import db
    db.init_app(app)


def configure_blueprints(app):
    """
    Hàm khai báo các URL prefix.
    :param app: Đối tượng flask app.
    :return:
    """
    api.init_app(app)


def configure_log_handlers(app):
    """
    Config log
    :param app: flask app
    :return: not return
    """
    fmt = '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
    formatter = logging.Formatter(fmt)

    info_log = os.path.join(app.config['LOG_FOLDER'], app.config['INFO_LOG'])
    info_file_handler = logging.handlers.RotatingFileHandler(info_log,
                                                             maxBytes=100000,
                                                             backupCount=10)
    info_file_handler.setLevel(logging.DEBUG)
    info_file_handler.setFormatter(formatter)
    app.logger.addHandler(info_file_handler)

    error_log = os.path.join(app.config['LOG_FOLDER'], app.config['ERROR_LOG'])
    error_file_handler = logging.handlers.RotatingFileHandler(error_log,
                                                              maxBytes=100000,
                                                              backupCount=10)
    error_file_handler.setLevel(logging.ERROR)
    error_file_handler.setFormatter(formatter)
    app.logger.addHandler(error_file_handler)

    handler_console = logging.StreamHandler(stream=sys.stdout)
    handler_console.setFormatter(formatter)
    handler_console.setLevel(logging.INFO)
    app.logger.addHandler(handler_console)

    # set proper log level
    app.logger.setLevel(logging.DEBUG if app.debug else logging.ERROR)

    # unify log format for all handers
    for h in app.logger.handlers:
        h.setFormatter(formatter)

    app.logger.info('Config filename: {0}'.format(app.config['FILENAME']))
    app.logger.info('App log folder: {0}'.format(app.config['LOG_FOLDER']))


def configure_error_handlers(app):
    """Configures the error handlers."""

    for exception in default_exceptions:
        app.register_error_handler(exception, api_error_handler)
    app.register_error_handler(Exception, api_error_handler)


# def init_indexs(mongo):
#     """
#     Create index for VSM
#     ex:
#         mongo.db.collection_name.create_index('key')
#         mongo.db.collection_name.create_index([('key', ASCENDING)])
#         mongo.db.collection_name.create_index([(‘key1′, ASCENDING), (‘key2′, DESCENDING)])
#     """
#     created_index = mongo.db.config.find_one(
#         {"created_index": {"$in": [True, False]}}, {'id': 0})
#
#     if not created_index or not created_index.get('created_index', False):
#         # mongo.db.nvd.create_index('cve_id')
#         mongo.db.config.update({"created_index": {"$in": [True, False]}},
#                                {'$set': {'created_index': True}}, True)


def init_i18n(mongo):
    """
    Create i18n db for VSM
    """
    # create i18n db
    i18n_db = mongo.db.i18n.find_one()
    # if i18n_db is None:
    #     mongo.db.i18n.insert(I18N_DATA)


def configure_after_request(app):
    @app.after_request
    def after_request(response):
        return response


# def export_swagger_specifications():
#     from flask import json
#     from .api import demo
#
#     with open("swagger.json", "w") as f:
#         f.write(json.dumps(demo.api.__schema__))
