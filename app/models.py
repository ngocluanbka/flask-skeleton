from .extensions import db
from datetime import datetime


class MethodsMixin(object):
    """
    This class mixes in some common Class table functions like
    delete and save
    """

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    def delete(self):
        ret = self.id
        db.session.delete(self)
        db.session.commit()
        return ret


class Experience(db.Model, MethodsMixin):
    __tablename__ = "demo"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    field1 = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __init__(self, **kwargs):
        keys = ['field1']
        for key in keys:
            setattr(self, key, kwargs.get(key))