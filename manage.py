# -*- coding: utf-8 -*-
import sys, os
# add root folder and lib to Python Path
sys.path.append(os.path.dirname(sys.path[0]))
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'lib'))

from flask_script import Manager
from app.flask_app import create_app, configure_app, DEFAULT_GLOBAL_CONFIG_FILE
from app.extensions import db
from flask import url_for


# init Flask app
app = create_app()
manager = Manager(app)


# run api using Flask-Script
@manager.option('-c', '--config', dest='config_file', default=DEFAULT_GLOBAL_CONFIG_FILE)
def runserver(config_file):
    """Run in local machine."""
    configure_app(app, filename=config_file)

    if app.config.get("DEBUG"):
        app.run(host='0.0.0.0', port=8890, use_reloader=True, threaded=False)
    else:
        app.run(host='0.0.0.0', port=8890, use_reloader=False, threaded=True)


@manager.command
def list_routes():
    import urllib
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
        output.append(line)

    for line in sorted(output):
        print(line)
import sys, os

# add root folder and lib to Python Path
sys.path.append(os.path.dirname(sys.path[0]))
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'lib'))
#
# @manager.command
# def mars_init():
#     from backend.app.lib.create_first_sadmin import create_first_sadmin, flushdb, register_role
#     configure_app(app, filename=DEFAULT_GLOBAL_CONFIG_FILE)
#     flushdb(app)
#     createdb(app)
#     register_role(app)
#     create_first_sadmin(app)


def createdb(app):
    """
    Initially create the tables in the database. The database must exist.
    (SQLite database will be created)
    """
    # configure_app(app, filename=DEFAULT_GLOBAL_CONFIG_FILE)
    with app.app_context():
        print(db)
        db.create_all()
        db.session.commit()


# run api using WSGI
def build_app(config_file=DEFAULT_GLOBAL_CONFIG_FILE):
    configure_app(app, filename=config_file)
    return app


if __name__ == "__main__":
    import os
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    manager.run(default_command='runserver')